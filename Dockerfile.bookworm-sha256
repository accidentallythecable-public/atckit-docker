# Copyright 2023-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of AccidentallyTheCables Docker Images,
# and is released under "GPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###
FROM debian:bookworm-slim

ENV DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

RUN apt update && \
    apt -y install python3 python3-pip python3-venv && \
    python3 -m pip install --upgrade --break-system-packages pip && \
    python3 -m pip install --upgrade --break-system-packages build twine pylint mypy && \
    rm -rf /var/lib/apt/lists/ /var/cache/apt
RUN apt update && \
    apt -y install make git curl jq shellcheck && \
    rm -rf /var/lib/apt/lists/ /var/cache/apt
RUN echo "[core]\n\trepositoryformatversion = 1\n[extensions]\n\tobjectformat = sha256" > ~/.gitconfig

CMD ["bash"]
