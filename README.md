# AccidentallyTheCables Docker Images

- [AccidentallyTheCables Docker Images](#accidentallythecables-docker-images)
  - [Installed Packages](#installed-packages)
    - [OS Packages](#os-packages)
    - [Python Packages](#python-packages)
  - [Images](#images)
      - [`atckit-docker:bookworm-*`](#atckit-dockerbookworm-)
      - [`atckit-docker:bullseye-*`](#atckit-dockerbullseye-)
    - [Adding Images](#adding-images)
  - [Building](#building)

Version numbers are based on changes here, not OS changes

## Installed Packages

### OS Packages

 - python3
 - python3-pip
 - python3-venv
 - make
 - git
 - curl
 - jq
 - shellcheck

### Python Packages

 - build
 - twine
 - pylint
 - mypy

## Images

#### `atckit-docker:bookworm-*`

Base: `debian:bookworm-slim`
Registry: `registry.gitlab.com`
URL: `accidentallythecable-public/atckit-docker/atckit-docker:bookworm-*`

Debian Bookworm with basic requirements installed (see [Installed Packages](#installed-packages))

#### `atckit-docker:bullseye-*`

Base: `debian:bullseye-slim`
Registry: `registry.gitlab.com`
URL: `accidentallythecable-public/atckit-docker/atckit-docker:bookworm-*`

Debian Bullseye with basic requirements installed (see [Installed Packages](#installed-packages))

### Adding Images

Images need to be added to `dockerfile.targets` in order to be built.

## Building

Requires AccidentallyTheCables Automation Tools; see `make docker_help`
